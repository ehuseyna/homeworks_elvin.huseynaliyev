import java.util.Arrays;

public class Human {

    public String name;
    public String surname;
    int year; // number
    int iq; // a whole number from 1 to 100))
    Pet pet;
    Human mother;
    Human father;
    String[][] schedule = new String[7][2]; // 2d array: [day of the week] x [type of the activity]

    public String toString() {
        return  "Human{name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", mother=" + mother.name + " " +  mother.surname+
                ", father=" + father.name + " " + father.surname+
                ", pet=" + String.valueOf(pet) + '}';
    }


    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;

    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.schedule = schedule;
    }


    public Human() {
    }








public void greetPet () {
        System.out.println("Hello " + pet.nickname);
    }

    public void describePet () {
    if ( pet.trickLevel > 50 )
        System.out.println("I have a " + pet.species + " he is " + pet.age + " years old, he is very sly");
    else
        System.out.println("I have a " + pet.species + " he is" + pet.age + " years old, he is almost sly"); }





}
