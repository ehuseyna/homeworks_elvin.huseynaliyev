import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;
import java.util.Random;

public class homework1 {
    public static void main(String[] args) {

        String [][] yearEvent = new String [5][5];
        yearEvent [0][0] = "When did the World War II begin?.";
        yearEvent [0][1] = "1939";
        yearEvent [1][0] = "When Azerbaijan became independent";
        yearEvent [1][1] = "1991";
        yearEvent [2][0] = "When was I born";
        yearEvent [2][1] = "1993";
        yearEvent [3][0] = "In which century Cortes conquer Mexico?";
        yearEvent [3][1] = "16";
        yearEvent [4][0] = "When twin towers were destroyed?";
        yearEvent [4][1] = "2001";




        System.out.println("Let the game begin! ");
        Scanner inputName = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter your name");
        String name = inputName.nextLine(); // user inputs his/her name

        int[] enteredNumbers = new int[100];
        int number=0;
        int counter = 0;
        boolean k = true;
        while (k) {  //checks if k=true, if false loop stops

            Random random =  new Random();
            int randomNumber = random.nextInt(5); // Generating Random Numbers in a Range 0-5
            System.out.println("random number is: " + randomNumber);
            Integer yearOfEvent = Integer.valueOf(yearEvent[randomNumber][1]);      //convert string to int


            while (true) {

                System.out.println( yearEvent[randomNumber][0]);

                Scanner inputNumber = new Scanner(System.in);  // Create a Scanner object
                if( inputNumber.hasNextInt()) {    // checks if input is number
                    number= inputNumber.nextInt(); //user enters number
                    enteredNumbers[counter]=number; //stores entered numbers in array
                    break;
                }
                else
                    System.out.println("This is not a number");
            }



              if ( number < yearOfEvent ) System.out.println("Your number is too small. Please, try again");
              if (number > yearOfEvent )  System.out.println("Your number is too big. Please, try again");
              if(number == yearOfEvent) {
                System.out.println("Congratulations " + name + "!");
                k = false;

                    int a, b, t;
                    int size = enteredNumbers.length;
                      for(a=1; a < size; a++)  //bubble sort to organize the array: lowest to highest
                      for(b=size-1; b >= a; b--){
                          if(enteredNumbers[b-1] > enteredNumbers[b]){
                              t = enteredNumbers[b-1];
                              enteredNumbers[b-1] = enteredNumbers[b];
                              enteredNumbers[b] = t;
                          }
                      }
                   for(int i = 0; i < size / 2; i++)  //  reverse the organized array: highest to lowest
                   {
                      int temp = enteredNumbers[i];
                      enteredNumbers[i] = enteredNumbers[size - i - 1];
                      enteredNumbers[size - i - 1] = temp;
                     }

                  // finally, print the  entered numbers in order ( from larger to smaller )
                   for (int j = 0; j < counter+1; j++)  System.out.print(enteredNumbers[j] + " ");

              }

            counter++;

        }



    }

}
